package com.orion.smart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/10
 * @since JDK1.8
 */

@RestController
@RequestMapping("/redis/zset")
public class RedisZSetController {


  private final RedisTemplate redisTemplate;

  @Autowired
  public RedisZSetController(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  /**
   * redis 操作list
   */
  @PostMapping
  public void setZSet() {
    //添加
    redisTemplate.opsForZSet().add("name", "zhaowu", 1.0);
  }

  /**
   * redis 操作list
   */
  @GetMapping
  public void getZSet() {
    Object result = null;
    Cursor cursor = redisTemplate.opsForZSet().scan("name", ScanOptions.NONE);
    while (cursor.hasNext()) {
      result = cursor.next();
      logger.info(String.valueOf(result));
    }
    logger.info(String.valueOf(result));
  }

  /**
   * 删除list 中的值
   */
  @DeleteMapping
  public void remove() {
    //移除一个或多个元素
    redisTemplate.opsForZSet().remove("name", "zhaowu");
  }


  private Logger logger = LoggerFactory.getLogger(RedisListController.class);
}
