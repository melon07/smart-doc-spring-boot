**smart-doc**是一个java restful api文档生成工具，smart-doc颠覆了传统类似swagger这种大量采用注解侵入来生成文档的实现方法。smart-doc完全基于接口源码分析来生成接口文档，完全做到零注解侵入，**只需要按照java标准注释的写就能得到一个标准的markdown接口文档**。如果你已经厌倦了swagger等文档工具的注解和强侵入污染，那请拥抱smart-doc吧！目前smart-doc正在完善中。。。
添加依赖
```xml
        <dependency>
            <groupId>com.github.shalousun</groupId>
            <artifactId>smart-doc</artifactId>
            <version>1.1</version>
            <scope>test</scope>
        </dependency>
```
//严格模式下，会检测javadoc,如过没写注释会抛出异常
```java
/**
 * hello
 *
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/8/31
 * @since JDK1.8
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

  /**
   * 创建
   *
   * @param user
   * @return
   */
  @PostMapping
  public User create(User user) {
    return user;
  }

  /**
   * 查询
   *
   * @param name 用户名
   * @return
   */
  @GetMapping
  public User getName(String name) {
    return new User();
  }

  /**
   * 修改
   *
   * @param name
   * @return
   */
  @PutMapping
  public String update(String name) {
    return name;
  }

  /**
   * 删除
   *
   * @param name
   * @return
   */
  @DeleteMapping
  public String delete(String name) {
    return name;
  }
}

/**
 * 用户表
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/8/31
 * @since JDK1.8
 */
public class User {

  /**
   * 姓名
   */
  @NotNull//有此注解生成文档require = true
  private String name;

  /**
   * 电话
   */
  private String mobile;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
}
```
```java
/**
   * 测试生成文档
   */
  @Test
  public void testBuilderControllersApiSimple() {
    ApiConfig config = new ApiConfig();
    //服务地址
    config.setServerUrl("http://localhost:8010");
    //生成到一个文档
    config.setAllInOne(true);
    //采用严格模式
    config.isStrict();
    //文档输出路径
    config.setOutPath("/Users/dujf/Downloads/md");
    ApiDocBuilder.builderControllersApi(config);
    //将生成的文档输出到/Users/dujf/Downloads/md目录下，严格模式下api-doc会检测Controller的接口注释
  }
```
生成文档示例：
![image.png](https://upload-images.jianshu.io/upload_images/11748913-ec01faa2c7654cac.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

参考链接：https://github.com/shalousun/ApplicationPower/tree/master/smart-doc